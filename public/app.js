//List
var seedData = [
  {       'picture' : '../assets/pic1.jpg',
          'title' : 'Polka Dot Doris',
          'price' : '29.99',
          'description' : 'The Polka Dot Doris is a lovely designer dress that says adorable like nothing else in such a sweet & adorable way. With a gray polka dots alongside pastel yellow it has a color scheme for your little to stand out.',
          'detail_item' : 'Gray, Pastel Yellow & White',
          'about' : 'The Polka Dot Doris is a lovely designer dress that says adorable like nothing else in such a sweet & adorable way. With a gray polka dots alongside pastel yellow it has a monochromatic color schemefor your little to stand out.',
          'stock' : [
            {'3-6 Month' : '3'},
            {'6-12 Month' : '5'},
            {'12-18 Month' : '5'},
            {'18-24 Month' : '5'},
            {'2T' : '5'},
            {'3T' : '3'}
          ]
      },
      {   'picture' : '../public/pic2.jpg',
          'title' : 'Falling Floral Fiona',
          'price' : '29.99',
          'description' : 'The Falling Floral Flora is a lovely designer dress that says fall like nothing else in such a sweet & adorable way. With  a burnt orange alongside pastel orange & yellow it has a monochromatic color scheme mixed with the perfect accents of teal and green shades to pop.',
          'detail_item' : 'Gray, Pastel Yellow & White',
          'about' : 'The Falling Floral Flora is a lovely designer dress that says fall like nothing else in such a sweet & adorable way. With a burnt orange alongside a pastel orange & yellow it has a monochromatic color scheme mixed with the perfect accents of teal and green shades to pop. This Dress features an additional layer in the chest adding trendy geometric pattern to really catch the eye. Outfitted with snaps down the back for easy changing and folded shoulders for a cute modest twist.',
          'stock' : 15
      }];

//var url='https://goldenrm-testing.herokuapp.com/';
//var url='http://localhost:3000/';
var url='http://localhost:8080/';

/////////////////////
//  User METHODS  //
////////////////////

//Retrieve
var getSessionFromServer = function(success, failure) {
  return fetch(url+'session',{
    credentials: 'include'}).then(function(response) {
    if (response.status == 401) {
      failure();
    } else if (response.status == 200) {
      response.json().then(function (user) {
      success(user);
    });
    } else {
      console.log('Unexpected Server Error! Admin must debug.', res.status)
    }
  });
};

//Post
const sendUserToServer = function(newUser, success, exists) {
  return fetch(url+'users', {
    method: 'POST',
    body: JSON.stringify(newUser),
    credentials: 'include',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors'
  }).then(function(res) {
    if (res.status == 422){
      exists();
    } else if (res.status == 201) {
      success();
    } else {
      console.log('Unexpected Server Error!' + res.status + ' Admin must debug.');
    }
  });
};
//Update
const updateUserOnServer = function(User, success, failure) {
  return fetch(url+'products'+'/'+User._id, {
    method: 'PUT',
    body: JSON.stringify(User),
    credentials: 'include',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors'
  }).then(function (res) {
    if (res.status == 401) {
      failure();
    } else if (res.status == 200) {
      success();
    } else {
      console.log('Unexpected Server Error!' + res.status + ' Admin must debug.');
    }
  });
};
const sendSessionToServer = function(newSession, success, failure) {
  return fetch(url+'session', {
    method: 'POST',
    body: JSON.stringify(newSession),
    credentials: 'include',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors'
  }).then(function(res) {
    if (res.status == 401) {
      failure();
    } else if (res.status == 201) {
      res.json().then(function (user) {
      success(user);
    });
    } else {
      console.log('Unexpected Server Error!' + res.status + ' Admin must debug.');
    }
  });
};
const removeSessionFromServer = function(success, failure) {
  return fetch(url+'session', {
  method: 'DELETE',
  credentials: 'include',
  mode: 'cors'
}).then(function(res) {
    if (res.status == 401) {
      failure();
    } else if (res.status == 200) {
      success();
    } else {
      console.log('Unexpected Server Error!' + res.status + ' Admin must debug.');
    }
  });
}

////////////////////////
//  Product METHODS  //
///////////////////////

//List
var getProductsFromServer = function(success, failure) {
  return fetch(url+'products',{
    credentials: 'include'
  }).then(function(res) {
    if (res.status == 401) {
      failure();
    } else if (res.status == 200) {
      res.json().then(function(values){
        success(values);
      });
    } else {
      console.log('Unexpected Server Error!' + res.status + ' Admin must debug.');
    }
  });
};
//Retrieve
var getProductFromServer = function(mid) {
  return fetch(url+'products'+'/'+mid,{
    credentials: 'include'}).then(function(response) { //Add colon
    return response.json();
  });
};
//Post
const sendProductToServer = function(newProduct) {
  return fetch(url+'products', {
    method: 'POST',
    body: JSON.stringify(newProduct),
    credentials: 'include',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors'
  });
};
//Update
const updateProductOnServer = function(Product, success, failure) {
  return fetch(url+'products'+'/'+Product._id, {
    method: 'PUT',
    body: JSON.stringify(product),
    credentials: 'include',
    headers: new Headers({
      'Content-Type': 'application/json'
    }),
    mode: 'cors'
  }).then(function (res) {
    if (res.status == 401) {
      failure();
    } else if (res.status == 200) {
      success();
    } else {
      console.log('Unexpected Server Error!' + res.status + ' Admin must debug.');
    }
  });
};
//Destroy
const removeProductFromServer = function(mid) {
  return fetch(url+'products'+'/'+mid,{
  method: 'DELETE',
  credentials: 'include',
  mode: 'cors'
}).then(res => app.loadStuff());
};

var app = new Vue({
  el:'#app',
  data: {
    showEditForm: false,
    showCurrentProduct: false,
    showAccount: false,
    editAccount: false,
    currentSession: false,
    hasAccount: true,
    currentProduct: {},
    items: seedData,
    sessionStatus: true,
    invalidPass: false,
    newProduct: {
      picture: '',
      title: '',
      price: '',
      stock: '',
      description: '',
      detail_item:'',
      about: ''
    },
    newSession:{
      email: '',
      password: ''
    },
    newUser:{
      fName: '',
      lName: '',
      email: '',
      password: ''
    },
    currentUser:{},
    passwordCompare: '',
    emailExists: false
  },
  methods: {
    addUser: function () {
      sendUserToServer(this.newUser,
        function () {
          app.loadStuff();
          app.hasAccount = true;
          app.emailExists = false;
        },
        function () {
          app.emailExists = true;
          app.hasAccount = false;
          app.newUser = {
            fName: '',
            lName: '',
            email:'',
            password: ''
        }
      });
    },
    addSession: function () {
      sendSessionToServer(this.newSession,
        function(user) {
          app.currentUser = user;
          app.loadStuff();
          app.currentSession = true;
          app.invalidPass = false;
        },
        function () {
          app.currentSession = false;
          app.invalidPass = true;
          app.newSession = {
            email: '',
            password: ''
          }
        });
    },
    getSession: function () {
      getSessionFromServer(function(user) {
        app.currentUser = user;
        app.currentSession = true;
        },function () {
          currentSession = false;
      });
    },
    destroySession: function () {
      removeSessionFromServer(
        function() {
          app.loadStuff()
          app.currentSession = false;
        },
        function () {
          app.currentSession = false;
          app.loadStuff();
          console.log("Not Authenticated, Login")
      });
    },
    getUser: function(uid) {
      app.getSession()
      app.showAccount = true;
      app.showCurrentProduct = false;
      app.showEditForm = false;
      app.editAccount = false;
      app.loadStuff();
    },
    editUser: function(){
      app.editAccount = true;
      app.showAccount = false;
    },
    updateUser: function() {
      updateUserOnServer(this.currentUser, function () {
        app.emailExists = false;
        app.showEditUser = false;
        app.showUser = true;
      },
      function () {
        app.emailExists = true;
    });
  },
    getProduct: function (mid) {
      getProductFromServer(mid)
      .then(function(data) {
        console.log("Get Product:", data)
        app.product = data;
      });
    },
    addProduct: function () {
      sendProductToServer(this.newProduct)
      .then( res=> app.loadStuff());
      this.newProduct = {
        picture: '',
        title: '',
        price: '',
        stock: '',
        description: '',
        detail_item:'',
        about: ''
      };
    },
    updateProduct: function () {
      updateProductOnServer(this.currentProduct, function() {
        app.loadStuff();
        app.getProduct(mid);
        app.showEditForm = false;
        app.showCurrentProduct = true;
      }, function() {
        app.showEditForm = true;
        app.showCurrentProduct = false;
      });
    },
    destroyProduct: function (mid) {
      console.log("Destroying:", mid);
      removeProductFromServer(mid)
      .then(res => app.loadStuff());
    },
    //Show Product Data
    showProduct: function(product) {
      this.showEditForm = false;
      this.showCurrentProduct = true;
      this.currentProduct = product;
      app.showAccount = false;
      app.EditAccount = false;
    },
    //Show Update Form
    editProduct: function() {
      this.showEditForm = true;
      this.showCurrentProduct = false;
    },
    //Show post Product Form
    newProductForm: function() {
      this.showEditForm = false;
      this.showCurrentProduct = false;
    },
    switchToLogin: function() {
      this.hasAccount = true;
    },
    switchToRegister: function () {
      this.hasAccount = false;
    },
    loadStuff: function () {
      getProductsFromServer(
        function(data) {
          app.currentSession = true;
          console.log("currectSessionValue: ", app.currentSession, "Data: ", data);
          app.items = data;
      }, function () {
        app.currentSession = false;
      });
    }
  },
  computed: {
    account: function() {
      return app.showAccount == true || app.editAccount == true;
    },
    emailFormatInvalid: function() {
      emailInvalid = true;
      email = this.newUser.email
      for (i=0; i< email.length; i++) {
        if  (email[i] == "@") {
          emailInvalid = false;
        }
      }
      return emailInvalid;
    },
    emailAlreadyExists: function() {
      return this.emailExists == true;
    },
    lowercaseLetterRequired: function() {
      letters = "abcdefghijklmnopqrstuvwxyz";
      noLowercase = true;
      for (i=0; i< app.newUser.password.length; i++) {
        for (j=0; j< letters.length; j++) {
          if  (app.newUser.password[i] == letters[j]) {
            noLowercase = false;
            return noLowercase;
          }
        }
      }
      return noLowercase;
    },
    capitalLetterRequired: function() {
      letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      noCapital = true;
      for (i=0; i< app.newUser.password.length; i++) {
        for (j=0; j< letters.length; j++) {
          if  (app.newUser.password[i] == letters[j]) {
            noCapital = false;
            return noCapital;
          }
        }
      }
      return noCapital;
    },
    symbolRequired: function() {
      symbols = "!@#$%^&*";
      noSymbol = true;
      for (i=0; i< app.newUser.password.length; i++) {
        for (j=0; j< symbols.length; j++) {
          if  (app.newUser.password[i] == symbols[j]) {
            noSymbol = false;
            return noSymbol;
          }
        }
      }
      return noSymbol;
    },
    numericValueRequired: function() {
      nums = "0123456789";
      noNumeric = true;
      for (i=0; i< app.newUser.password.length; i++) {
        for (j=0; j< nums.length; j++) {
          if  (app.newUser.password[i] == nums[j]) {
            noNumeric = false;
            return noNumeric;
          }
        }
      }
      return noNumeric;
    },
    passwordLengthInvalid: function() {
      return this.newUser.password.length < 8;
    },
    confirmPasswordNoMatch: function() {
      return this.newUser.password != app.passwordCompare;
    },
    noSession: function() {
      return this.currentSession == false;
    },
    validSession: function() {
      return this.currentSession == true;
    },
    emailOrPasswordInvalid: function(){
      return this.invalidPass == true;
    }
  },
  created: function () {
    this.loadStuff();
  }
});
