var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

mongoose.connect('mongodb://byu:pioneer47@ds137483.mlab.com:37483/goldenrm-testing', { useNewUrlParser: true });

var userSchema = new mongoose.Schema({
  fname: {
    type: String,
    required: [true, "Please add a First Name"]
  },
  lname: String,
  role: String,
  email: {
    type: String,
    unique: true
  },
//  Type: String, //freemium, premium, moderator, developer, superDeveloper
  encryptedPassword: String
});
/*
var cartSchema = new mongoose.Schema({
  productID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
  },
  quantity: Number
});
*/
var productSchema = new mongoose.Schema({
  picture: String,
  title: String,
  price: Number,
//  stock: Array,
  stock: Number,
  description: String,
  detail_item:String,
  about: String
});

userSchema.methods.setPassword = function (plainPassword, callback) {
console.log("Enterd setPassword user method, passed plainPassword: ", plainPassword);
var user = this;
console.log("No Fail 1", plainPassword);
bcrypt.hash(plainPassword, 13).then(function(hash) {
    user.encryptedPassword = hash;
    console.log("Password Encrypted", user.encryptedPassword);
    callback(); // Review this function
  });
};

userSchema.methods.validPassword = function (plainPassword, callback) {
  console.log("Validating yo' Password", plainPassword, this.encryptedPassword)
  bcrypt.compare(plainPassword, this.encryptedPassword)
  .then(function(valid) {
    console.log('validated');
    callback(valid);
  });
};

userSchema.methods.simpleUser = function () {
  return {
    fname: this.fname,
    lname: this.lname,
    email: this.email,
    role: this.role,
    _id: this._id
  };
};
/*a return is synchronous -- a function call is asynchronous (callback)
cartSchema.statics.howManyProducts = function () {
  return this.count();
};
*/
var Product = mongoose.model('Product', productSchema);
//var Cart = mongoose.model('Cart', cartSchema);
var User = mongoose.model('User', userSchema);
module.exports = {
  Product: Product,
//  Cart: Cart,
  User: User,
};
