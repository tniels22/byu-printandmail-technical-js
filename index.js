var model = require('./public/model.js');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var passport = require('passport');
var passportLocal = require('passport-local');
var util = require('util');

var app = express();

// Passing JSON DATA
app.use(bodyParser.json());
//Enable Static Files
app.use(express.static('public'));

//Enable cors to enable JSON
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//////////////
// Sessions //
/////////////

//// Middleware to add for Passport ////
app.use(session({secret: 'TerranZergProtoss', resave: false, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new passportLocal.Strategy({
  usernameField: 'email'
}, function(email, password, done) {
    model.User.findOne({ email: email }).then( function (user) {
      if (!user) {
        return done(null, false);
      }
      user.validPassword(password, function(valid) {
        if (valid){
          return done(null, user); // The user is returned for session data
        } else{
          return done(null, false);
        }
      });
    }, function (err) {
      return done(err);
    });
  }));

passport.serializeUser(function(user, done) {
  console.log('Serialize user called.');
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  console.log('Deserialize user called.');
  model.User.findOne({ _id: id}).then(function (user){
    done(null, user);
  }, function(err) {
    done(err);  //user not found
  });
});
////////////////////////
//// Session ROUTES ////
///////////////////////

// Authenticate Action -- /session is our singleton resource
///this means it is the member path there will be no collection path or ID
app.post('/session', passport.authenticate('local'),
function (req, res) {
  if (req.user){
    res.status(201).json(req.user.simpleUser());
    res.send(); //on Successful authentication
  }
  else{
    res.sendStatus(401);
  }
});

// singleton Me Action
app.get('/session', function (req, res) {
  if (req.user) {
    res.json(req.user.simpleUser());
  } else {
    res.sendStatus(401);
  }
});
// End Session
app.delete('/session', function (req, res) {
  if(req.user) {
    req.logout();
    res.sendStatus(200);
  } else{
    res.sendStatus(401); // send error by logout button that you are not logged in as fname
  }
});

/////////////////////
//// User ROUTES ////
/////////////////////

app.post('/users', function (req, res) {
  console.log('Post Route - request received:', req.body.email);
  model.User.findOne({ email: req.body.email }).then( function (user) {
    if (user) {
      res.sendStatus(422).json('User exists');
    }
    else{
      var user = new model.User({
        fname: req.body.fName,
        lname: req.body.lName,
        email: req.body.email
      });
      console.log("Before Set Password REQUEST:", req.body, "EMAIL:",req.body.email, "PASSWORD:",req.body.password);
      user.setPassword(req.body.password, function() {
        user.save().then(function(){
          console.log("save Worked");
          res.set('Access-Control-Allow-Origin', '*');
          res.sendStatus(201);
        });
      });
    }
  })
});
app.put('/users', function (req, res) {
  model.User.findOne({'_id' : req.params.id}).then( function (user) {
    if (user.email != req.body.email) {
      model.User.findOne({'_id' : req.params.id}).then( function (user) {
        if (user) {
          res.sendStatus(422).json('User exists');
        }
      })
    }
  })
  model.User.findOneAndUpdate({'_id' : req.params.id},
    { $set: { 'fname': req.body.fname,
              'lname': req.body.lname,
              'email': req.body.email
              }
    });
    user.setPassword(req.body.password, function() {
    }).user.setPassword(req.body.password, function() {
      user.update().then(function(){
        console.log("save Worked");
        res.set('Access-Control-Allow-Origin', '*');
        res.sendStatus(201);
      });
    });
});
////////////////////////
//// Product ROUTES ////
////////////////////////

app.get('/products', function (req, res) {
  if (req.user) {
    model.Product.find().then(function(products){
      res.set('Access-Control-Allow-Origin', '*');
      res.json(products);
    });
  }
  else{
    res.sendStatus(401);
  }
});

app.get('/products/:id', function (req, res){
  if (req.user) {
    model.Product.findById(req.params.id).then(function(product){
      res.set('Access-Control-Allow-Origin', '*');
      res.json(product);
    });
  } else {
    res.sendStatus(401);
  }
});

app.post('/products', function (req, res) {
  console.log("post:/products --> Current User", req.user);
  var Product = new model.Product({
    picture: req.body.picture,
    title: req.body.title,
    price: req.body.price,
    stock: req.body.stock,
    description: req.body.description,
    detail_item: req.body.detail_item,
    about: req.body.about
  });
  Product.save().then(function(){
    res.set('Access-Control-Allow-Origin', '*');
    res.sendStatus(201);
  });
});

app.put('/products/:id', function (req, res) {
  if (req.user) {
    model.Product.findOneAndUpdate({'_id' : req.params.id},
      { $set: { 'picture': req.body.picture,
                'title': req.body.title,
                'price': req.body.price,
                'stock': req.body.stock,
                'description': req.body.description,
                'detail_item': req.body.detail_item,
                'about': req.body.about}
      }).then(function(err, products){
      if (err) return res.json({Error: err});
        res.json(products);
    });
  } else {
    res.sendSatus(401);
  }
});

app.delete('/products/:id', function (req, res) {
  if (req.user) {
    model.Product.findOneAndRemove({'_id' : req.params.id})
    .then(function(err, products){
      if (err) return res.json({Error: err});
        res.json({success:true});
    });
  } else {
    res.sendSatus(401);
  }
});

//// Cart ROUTES ////

//Still determining if necessary
app.post('/cart', function (req, res) {
  if (!req.user) {
    req.sendStatus(401);
    return;
  }
  var cart = new model.Cart({
    pid: req.body.pid,
    quantity: req.body.quantity
  });
  Cart.save().then(function(){
    res.set('Access-Control-Allow-Origin', '*');
    res.sendStatus(201);
  });
});


// On delete if they don't match the UID  then its 403 unauthorized... logging in won't help.
 app.listen(8080, function () {
//app.listen(process.env.PORT || 3000, function () {
   console.log("I'm Ready! I'm Ready! I'm Ready");
});

//Need to SANITIZE My DATA BEFORE SAVING
