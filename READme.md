#Running the Code
1. run with node index.js in main directory of web app
2. View in browser at: localhost:8080

# Resources:
1. Products
2. Users
3. Session

# Rest End Points

request | Path | Method
----|----|----
LIST| /Products| GET
RETRIEVE | /Products/id| GET
CREATE| /Products| POST
UPDATE | /Products/id|PUT
DEL| /Products/id| DELETE
RETRIEVE | /Session | GET
CREATE | /Session | POST
DELETE | /Session | DELETE
CREATE | /users | POST


# User Attributes:
1. _id
2. fName
3. lName
4. email
5. role
6. password

# Product Attributes:
1. _id
2. picture
3. title
4. price
5. stock
6. description
7. detail_item
8. about

# Session Attributes:
1. _id
